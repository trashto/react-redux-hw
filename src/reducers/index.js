import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, TOGGLE_MODAL } from "../messages/actionTypes";

const initialState = {
  chat: {
    messages: [],
    editModal: false,
    messageToUpdate: null,
    preloader: true,
  }
}

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_MESSAGE: {
      const { id, data } = action.payload;
      const newMassage = { id, ...data };
      const inSt = { ...state }
      const isIdUnique = !inSt.chat.messages.some(message => message.id === id);
      if (isIdUnique) {
        inSt.chat.messages = [...state.chat.messages, newMassage]
        return inSt;
      }
      return state;
    }
    case UPDATE_MESSAGE: {
      const { id, data } = action.payload;
      const inSt = { ...state };
      const updateMessages = state.chat.messages.map(message => {
        if (message.id === id) {
          const messageCopy = { ...message }
          messageCopy.text = data;
          messageCopy.editedAt = new Date();
          return messageCopy
        } else {
          return message;
        }
      });
      inSt.chat.messages = [...updateMessages]
      return inSt;
    }
    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMasseges = state.chat.messages.filter(message => message.id !== id);
      const inSt = { ...state };
      inSt.chat.messages = [...filteredMasseges];
      return inSt;
    }
    case TOGGLE_MODAL: {
      const { id } = action.payload;
      const inSt = { ...state };
      inSt.chat.messageToUpdate = state.chat.messages.find(message => message.id === id);
      inSt.chat.editModal = !inSt.chat.editModal;
      return inSt;
    }

    default:
      return state;
  }
}

export default rootReducer;