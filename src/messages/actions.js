import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, TOGGLE_MODAL } from "./actionTypes";
import { getNewId } from "./service";

export const addMessage = data => ({
  type: ADD_MESSAGE,
  payload: {
    id: data.id ?? getNewId(),
    data
  }
});

export const updateMessage = (id, data) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    data
  }
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
})

export const toggleModal = (id) => ({
  type: TOGGLE_MODAL,
  payload: {
    id
  }
})