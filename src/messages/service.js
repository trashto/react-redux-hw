export const getNewId = () => Math.random().toString(36).substr(2, 9);

export const getNormalTime = (date) => {
  const hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
  const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
  return hours + ':' + minutes
}