import React from "react"
import "../styles/MessageList.css"
import Message from "./Message"
import OwnMessage from "./OwnMessage ";
import * as actions from "../messages/actions"
import { connect } from "react-redux";

class MessageList extends React.Component {

  renderAllMessages() {
    const data = this.props.data.chat.messages;
    data.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
    data.reverse();
    const messages = data.map((message, i) => {
      return (!(message.userId === 'itsMeMario')
        ?
        <Message
          key={i} messageData={message}
        />
        :
        <OwnMessage
          key={i} messageData={message}
          toggleModal={this.props.toggleModal}
          deleteMessage={this.props.deleteMessage}
        />)
    });
    return messages
  }
  render() {
    return (
      <div className='message-list' >
        {this.renderAllMessages()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state
  }
}
const mapDispatchToProps = {
  ...actions,
}
export default connect(mapStateToProps, mapDispatchToProps)(MessageList)