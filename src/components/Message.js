import React from "react"
import "../styles/Message.css"
import { getNormalTime } from "../messages/service"

class Message extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLiked: false,
    }
  }
  handleLike() {
    this.setState({
      isLiked: !this.state.isLiked,
    })
  }
  getTime(){
    const date = new Date(this.props.messageData.createdAt)
    return getNormalTime(date)
  }
  render() {
    return (
      <div className='message'>
        <div>
          <div className='user-info'>
            <img className='message-user-avatar' width='65px' height='60px' alt='avatar' src={this.props.messageData.avatar} />
            <p className='message-user-name'>{this.props.messageData.user}</p>

          </div>
          <p className='message-text'>{this.props.messageData.text}</p>
        </div>
        <div className='addition-opt'>
          <span className='message-time'>{this.getTime()}</span>
          <button className={this.state.isLiked ? 'message-liked' : 'message-like'} onClick={() => this.handleLike()}>Like</button>
        </div>
      </div>
    )
  }
}

export default Message