import React from "react"
import "../styles/MessageInput.css"

class MessageInput extends React.Component {
  setText(e) {
    this.setState({
      text: e.target.value
    });
  }
  onArrowUp(e) {
    if (e.code === 'ArrowUp') {
      this.props.updateLatestMessage()
    }
  }
  onSend(text) {
    const message = {
      text: text,
      createdAt: new Date(),
      editedAt: "",
      user: "Own",
      userId: "itsMeMario",
    }
    this.props.addMessage(message)
  }
  
  render() {
    return (
      <div className='message-input'>
        <textarea className='message-input-text' type='text'
          onKeyDown={(e) => this.onArrowUp(e)}
          onChange={(e) => this.setText(e)} ></textarea>
        <button className='message-input-button'
          onClick={() => this.onSend(this.state.text)}>Send</button>
      </div>
    )
  }
}

export default MessageInput