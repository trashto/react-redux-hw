import React from "react"
import Header from "./Header";
import Preloader from "./Preloader";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import EditModal from "./EditModal";
import { getNormalTime } from "../messages/service"
import "../styles/Chat.css"
import * as actions from "../messages/actions"
import { connect } from "react-redux";

class Chat extends React.Component {
  constructor(props) {
    super(props)
  }


  componentDidMount() {
    const url = this.props.url;
    fetch(url)
      .then(response => response.json())
      .then(messages => messages.forEach((message) => {
        this.props.addMessage(message)
        this.props.data.chat.preloader = false;
      }))
      .catch(error => console.log(error));
  }


  getInfoForHeader() {
    const data = this.props.data.chat.messages;
    const latestDate = new Date(Math.max(...data.map(d => new Date(d.createdAt))));
    const users = [];
    data.forEach(message => {
      if (!users.includes(message.user)) users.push(message.user);
    })
    return {
      usersCount: users.length,
      messagesCount: data.length,
      lastMessageDate: `${latestDate.getDay()}.${latestDate.getMonth()}.${latestDate.getFullYear()} ${getNormalTime(latestDate)}`
    }
  }
  updateLatestMessage() {
    const chat = this.props.data.chat;
    const ownMessages = chat.messages.filter(message => message.userId === 'itsMeMario');
    if (!ownMessages.length) {
      return;
    }
    const lastMessage = ownMessages[ownMessages.length - 1];
    this.props.toggleModal(lastMessage.id)
  }

  render() {
    const chat = this.props.data.chat;
    return (
      <div className={chat.preloader ? 'chat' : 'chat visible'}>

        <EditModal
          messageToUpdate={chat.messageToUpdate}
          toggle={this.props.toggleModal}
          editModal={chat.editModal}
          updateMessage={this.props.updateMessage}
        />

        <Preloader preloader={chat.preloader} />
        <Header data={this.getInfoForHeader()} />
        <MessageList data={chat.messages} />
        <MessageInput
          addMessage={this.props.addMessage}
          updateLatestMessage={() => this.updateLatestMessage()}
        />
      </div>
    )
  }

}


const mapStateToProps = (state) => {
  return {
    data: state
  }
}
const mapDispatchToProps = {
  ...actions,
}
export default connect(mapStateToProps, mapDispatchToProps)(Chat)