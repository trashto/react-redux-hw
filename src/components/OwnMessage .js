import React from "react"
import "../styles/OwnMessage.css"
import { getNormalTime } from "../messages/service"

class OwnMessage extends React.Component {
  onEdit() {
    const data = this.props.messageData;
    this.props.toggleModal(data.id)
  }
  onDelete() {
    const data = this.props.messageData;
    this.props.deleteMessage(data.id)
  }
  render() {
    const data = this.props.messageData;
    return (
      <div className='own-message'>
        <p className='message-text'>{data.text}</p>
        <div className='addition-opt'>
          <span className='message-time'>{getNormalTime(this.props.messageData.createdAt)}</span>
          <button className='message-edit' onClick={() => this.onEdit()}>Edit</button>
          <button className='message-delete' onClick={() => this.onDelete()}>Delete</button>
        </div>
      </div>
    )
  }
}

export default OwnMessage