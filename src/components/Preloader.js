import React from "react";
import "../styles/Preloader.css"

class Preloader extends React.Component {
  render() {
    return (
      <div className={this.props.preloader ? 'preloader' : 'preloader hidden'}>
      </div>
    )
  }
}

export default Preloader