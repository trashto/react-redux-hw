import React from "react"
import "../styles/Header.css"

class Header extends React.Component {
 
  render() {
    return (
      <header className='header'>
        <h1 className='header-title'>MyChat</h1>
        <div className='header-users-count'>{this.props.data.usersCount}</div>
        <div className='header-messages-count'>{this.props.data.messagesCount}</div>
        <div className='header-last-message-date'>{this.props.data.lastMessageDate}</div>
      </header>
    )
  }
}

export default Header