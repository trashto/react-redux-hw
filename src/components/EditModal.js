import React from "react"
import "../styles/EditModal.css"

class EditModal extends React.Component {

  setText(e) {
    this.setState({
      text: e.target.value
    })
  }
  onSave() {
    const id = this.props.messageToUpdate.id;
    this.props.updateMessage(id, this.state.text)
    this.props.toggle(null);
  }
  
  onClose() {
    this.props.toggle(null);
  }

  render() {
    return (
      <div className={!this.props.editModal ? 'edit-message-modal' : 'edit-message-modal modal-shown'}>
        <textarea className='edit-message-input' onChange={(e) => this.setText(e)} defaultValue={this.props.messageToUpdate?.text}></textarea>
        <div className='controls'>
          <button className='edit-message-button' onClick={() => this.onSave()}>Save</button>
          <button className='edit-message-close' onClick={() => this.onClose()}>Close</button>
        </div>
      </div>
    )
  }
}

export default EditModal