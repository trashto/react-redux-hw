import React from 'react';
import Chat from './components/Chat'
import './App.css';

function App() {
  return (
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json"/>
  );
}

export default App;
