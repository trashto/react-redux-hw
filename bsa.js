import Chat from "./src/components/Chat";
import rootReducer from "./src/reducers";

export default {
  Chat,
  rootReducer,
}